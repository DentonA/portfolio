package projects.myProjectUniverse.view;

import projects.myProjectUniverse.model.Planet;
import projects.myProjectUniverse.model.Star;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Denton on (023) 23.02.16.
 */
public class Universe extends JPanel {
    private View view;

    public Universe(View view) {
        this.view = view;
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, view.getWidth(), view.getHeight());

        for (Star currentStar : view.getSpaceObjects().getStars()) {
            currentStar.draw(g);
            for (Planet currentPlanet : currentStar.getPlanets())
                currentPlanet.draw(g);
        }
    }
}
