package projects.myProjectUniverse.view;

import projects.myProjectUniverse.controller.Controller;
import projects.myProjectUniverse.model.SpaceObjects;

import javax.swing.*;

/**
 * Created by Denton on (023) 23.02.16.
 */
public class View extends JFrame {
    private Controller controller;
    private Universe universe;

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {
        universe = new Universe(this);
        add(universe);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(1200, 700);
        setLocationRelativeTo(null);
        setTitle("My universe simulator");
        setVisible(true);
    }

    public SpaceObjects getSpaceObjects() {
        return controller.getSpaceObjects();
    }
}
