package projects.myProjectUniverse.controller;

import projects.myProjectUniverse.model.Model;
import projects.myProjectUniverse.model.SpaceObjects;
import projects.myProjectUniverse.view.View;

/**
 * Created by Denton on (023) 23.02.16.
 */
public class Controller {
    private View view;
    private Model model;

    public Controller() {
        view = new View(this);
        model = new Model(view);
        view.init();
        model.restart();
    }

    public static void main(String[] args) {
        Controller controller = new Controller();
    }

    public SpaceObjects getSpaceObjects() {
        return model.getSpaceObjects();
    }
}