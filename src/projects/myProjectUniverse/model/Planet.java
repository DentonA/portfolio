package projects.myProjectUniverse.model;

import projects.myProjectUniverse.Helper;

import java.awt.*;

/**
 * Created by Denton on (023) 23.02.16.
 */
public class Planet extends SpaceObject {
    public static final int PLANET_MIN_DIAMETER = 10;
    public static final int PLANET_MAX_DIAMETER = 30;

    private Star star;
    private int distanceToOrbit;
    private boolean clockwise;

    public Planet(Star star, int distanceToOrbit) {
        this.star = star;
        this.distanceToOrbit = distanceToOrbit;
        x = star.getX() - distanceToOrbit;
        y = star.getY();
        diameter = Helper.random(PLANET_MIN_DIAMETER, PLANET_MAX_DIAMETER);
        clockwise = Helper.random(0, 1) == 1; // 50% chances
    }

    @Override
    public void draw(Graphics graphics) {
        int leftX = x - diameter / 2;
        int upperY = y - diameter / 2;

        graphics.setColor(Color.DARK_GRAY);
        graphics.fillOval(leftX, upperY, diameter, diameter);
        graphics.setColor(Color.BLUE);
        graphics.fillOval(leftX + 3, upperY + 3, diameter - 6, diameter - 6);
    }

    @Override
    public void move() {

    }

    public void setDistanceToOrbit(int distanceToOrbit) {
        this.distanceToOrbit = distanceToOrbit;
    }
}
