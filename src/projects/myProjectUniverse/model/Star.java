package projects.myProjectUniverse.model;

import projects.myProjectUniverse.Helper;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Denton on (023) 23.02.16.
 */
public class Star extends SpaceObject {
    public static final int STAR_MIN_DIAMETER = 40;
    public static final int STAR_MAX_DIAMETER = 100;

    private ArrayList<Planet> planets = new ArrayList<>();

    public Star(int x, int y, int planetsCount) {
        this.x = x;
        this.y = y;
        diameter = Helper.random(STAR_MIN_DIAMETER, STAR_MAX_DIAMETER);
        for (int i = 0; i < planetsCount; i++) {
            int orbitSize = Helper.random(diameter + 10, diameter * 2); // [50 - 210]
            //if ()
            planets.add(new Planet(this, orbitSize));
        }
        color = Color.ORANGE; // todo: make random color gen
        lifetime = 100000; // todo: is this enough?
    }

    @Override
    public void draw(Graphics graphics) {
        int leftX = x - diameter / 2;
        int upperY = y - diameter / 2;

        graphics.setColor(Color.RED);
        graphics.fillOval(leftX, upperY, diameter, diameter);
        graphics.setColor(Color.ORANGE);
        graphics.fillOval(leftX + 2, upperY + 2, diameter - 4, diameter - 4);
        graphics.setColor(Color.YELLOW);
        graphics.fillOval(leftX + 6, upperY + 6, diameter - 12, diameter - 12);
    }

    @Override
    public void move() {

    }

    public ArrayList<Planet> getPlanets() {
        return planets;
    }
}
