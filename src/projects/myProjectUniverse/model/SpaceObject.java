package projects.myProjectUniverse.model;

import java.awt.*;

/**
 * Created by Denton on (023) 23.02.16.
 */
public abstract class SpaceObject {
    protected int x;
    protected int y;
    protected double speedX = 0;
    protected double speedY = 0;
    protected int diameter;
    protected Color color;
    protected int lifetime;

    public SpaceObject() {}

    public abstract void draw(Graphics graphics);

    public abstract void move();

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public double getSpeedX() {
        return speedX;
    }

    public void setSpeedX(double speedX) {
        this.speedX = speedX;
    }

    public double getSpeedY() {
        return speedY;
    }

    public void setSpeedY(double speedY) {
        this.speedY = speedY;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
