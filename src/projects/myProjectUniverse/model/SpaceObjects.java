package projects.myProjectUniverse.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Denton on (023) 23.02.16.
 */
public class SpaceObjects {
    Set<Star> stars = new HashSet<>();

    public SpaceObjects(Set<Star> stars) {
        this.stars = stars;
    }

    public Set<SpaceObject> getAll() {
        Set<SpaceObject> allObjects = new HashSet<>();
        allObjects.addAll(stars);

        return allObjects;
    }

    public Set<Star> getStars() {
        return stars;
    }
}
