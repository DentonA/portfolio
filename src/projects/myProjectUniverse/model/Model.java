package projects.myProjectUniverse.model;

import projects.myProjectUniverse.Helper;
import projects.myProjectUniverse.view.View;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Denton on (023) 23.02.16.
 */
public class Model {
    private SpaceObjects spaceObjects;
    private View view;

    public Model(View view) {
        this.view = view;
    }

    public SpaceObjects getSpaceObjects() {
        return spaceObjects;
    }

    public void restart() {
        Set<Star> stars = new HashSet<>();
        for (int i = 0; i < 3; i++) {
            int newStarX = Helper.random(Star.STAR_MIN_DIAMETER / 2, view.getWidth() - Star.STAR_MIN_DIAMETER / 2);
            int newStarY = Helper.random(Star.STAR_MIN_DIAMETER / 2, view.getHeight() - Star.STAR_MIN_DIAMETER / 2);
            System.out.println(newStarX + " / " + newStarY);
            stars.add(new Star(newStarX, newStarY, Helper.random(0, 3)));
        }
        spaceObjects = new SpaceObjects(stars);
    }
}
