package projects.snake;

import projects.snake.controller.Controller;

/**
 * Created by Denton on (029) 29.02.16.
 */
public class Game {
    private Controller controller;

    private static Game game;

    public static void main(String[] args) {
        game = new Game();
        game.init(20, 20);
        game.run();
    }

    public void init(int width, int height) {
        controller = new Controller(width, height);
    }

    public void run() {
        controller.run();
    }
}
