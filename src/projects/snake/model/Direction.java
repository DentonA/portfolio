package projects.snake.model;

/**
 * Created by Denton on (029) 29.02.16.
 */
public enum Direction {
    UP, DOWN, LEFT, RIGHT;
}
