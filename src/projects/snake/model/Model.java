package projects.snake.model;

import projects.snake.controller.Controller;

import java.util.ArrayList;

/**
 * Created by Denton on (029) 29.02.16.
 */
public class Model {
    private Controller controller;
    private Snake snake;
    private Mouse mouse;
    private int width;
    private int height;
    private static int[] levelDelay = {700, 600, 550, 500, 480, 460, 440, 420, 400, 380, 360, 340, 320, 300, 285, 270};

    public Model(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void init() {
        snake = new Snake(width / 2, height / 2);
        snake.setDirection(Direction.DOWN);
        snake.setNextStepDirection(Direction.DOWN);
        int mouseX;
        int mouseY;
        while (true) {
            mouseX = (int) (Math.random() * width);
            mouseY = (int) (Math.random() * height);
            if (mouseX != width / 2 && mouseY != height / 2)
                break;
        }
        mouse = new Mouse(mouseX, mouseY);
    }

    public void move() {
        SnakePart tail = snake.getSnakeParts().get(snake.getSnakeParts().size() - 1);
        snake.move();
        if (isHitWalls() || (snake.getSnakeParts().size() > 1 && isHitSelf())) {
            snake.setAlive(false);
            snake.getSnakeParts().remove(0);
            if (!snake.isBoosted())
                snake.getSnakeParts().add(tail);
            return;
        }
        snake.setBoosted(false);
        if (mouse.getX() == snake.getSnakeParts().get(0).getX() && mouse.getY() == snake.getSnakeParts().get(0).getY()) {
            snake.setBoosted(true);
            int mouseX;
            int mouseY;
            while (true) {
                mouseX = (int) (Math.random() * width);
                mouseY = (int) (Math.random() * height);
                boolean isSnakeCoord = false;
                for (SnakePart currPart : snake.getSnakeParts()) {
                    if (currPart.getX() == mouseX && currPart.getY() == mouseY) {
                        isSnakeCoord = true;
                        break;
                    }
                }
                if (!isSnakeCoord) break;
            }
            mouse = new Mouse(mouseX, mouseY);
        }
    }

    private boolean isHitWalls() {
        SnakePart head = snake.getSnakeParts().get(0);
        int newX = head.getX();
        int newY = head.getY();
        return newX < 0 || newY < 0 || newX >= width || newY >= height;
    }

    private boolean isHitSelf() {
        ArrayList<SnakePart> snakeParts = snake.getSnakeParts();
        int newX = snakeParts.get(0).getX();
        int newY = snakeParts.get(0).getY();
        boolean isHit = false;
        for (int i = 1; i < snakeParts.size(); i++) {
            if (snakeParts.get(i).getX() == newX && snakeParts.get(i).getY() == newY) {
                isHit = true;
                break;
            }
        }
        return isHit;
    }

    public void sleep() {
        try {
            int level = snake.getSnakeParts().size();
            int delay = level <= levelDelay.length ? levelDelay[level - 1] : 250;
            Thread.sleep(delay);
        } catch (InterruptedException e) {}
    }

    public Snake getSnake() {
        return snake;
    }

    public Mouse getMouse() {
        return mouse;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
