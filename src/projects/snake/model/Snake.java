package projects.snake.model;

import java.util.ArrayList;

/**
 * Created by Denton on (029) 29.02.16.
 */
public class Snake {
    private ArrayList<SnakePart> snakeParts = new ArrayList<>();
    private boolean isAlive;
    private boolean isBoosted;
    private Direction direction;
    private Direction nextStepDirection;

    public Snake(int x, int y) {
        snakeParts.add(new SnakePart(x, y));
        isAlive = true;
        isBoosted = false;
    }

    public void move() {
        if (!isAlive)
            return;
        direction = nextStepDirection;
        switch (direction) {
            case UP:
                move(0, -1);
                break;
            case DOWN:
                move(0, 1);
                break;
            case LEFT:
                move(-1, 0);
                break;
            case RIGHT:
                move(1, 0);
                break;
        }
    }

    private void move(int dx, int dy) {
        SnakePart head = snakeParts.get(0);
        head = new SnakePart(head.getX() + dx, head.getY() + dy);
        if (!isBoosted)
            snakeParts.remove(snakeParts.size() - 1);
        snakeParts.add(0, head);
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public boolean isBoosted() {
        return isBoosted;
    }

    public void setBoosted(boolean boosted) {
        isBoosted = boosted;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setNextStepDirection(Direction direction) {
        nextStepDirection = direction;
    }

    public ArrayList<SnakePart> getSnakeParts() {
        return snakeParts;
    }
}
