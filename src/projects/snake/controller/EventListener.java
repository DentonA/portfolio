package projects.snake.controller;

import projects.snake.model.Direction;

/**
 * Created by Denton on (029) 29.02.16.
 */
public interface EventListener {
    void onChangeDirectionEvent(Direction direction);
}
