package projects.snake.controller;

import projects.snake.model.Direction;
import projects.snake.model.Model;
import projects.snake.view.GUIView;
import projects.snake.view.View;

/**
 * Created by Denton on (029) 29.02.16.
 */
public class Controller implements EventListener {
    private View view;
    private Model model;

    public Controller(int width, int height) {
        this.model = new Model(width, height);
        this.view = new GUIView(model);
        view.init();
        model.init();
        model.setController(this);
        view.setEventListener(this);
    }

    public void run() {
        while (model.getSnake().isAlive()) {
            model.move();
            view.update();
            model.sleep();
        }
        view.finishGame();
    }

    public void onChangeDirectionEvent(Direction direction) {
        switch (direction) {
            case DOWN:
                if (model.getSnake().getDirection() == Direction.UP) return;
                break;
            case UP:
                if (model.getSnake().getDirection() == Direction.DOWN) return;
                break;
            case LEFT:
                if (model.getSnake().getDirection() == Direction.RIGHT) return;
                break;
            case RIGHT:
                if (model.getSnake().getDirection() == Direction.LEFT) return;
                break;
        }
        model.getSnake().setNextStepDirection(direction);
    }
}
