package projects.snake.view;

import projects.snake.controller.EventListener;

/**
 * Created by Denton on (029) 29.02.16.
 */
public interface View {
    void init();
    void update();
    void finishGame();
    void setEventListener(EventListener eventListener);
}
