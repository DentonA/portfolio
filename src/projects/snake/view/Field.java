package projects.snake.view;

import projects.snake.controller.EventListener;
import projects.snake.model.Direction;
import projects.snake.model.SnakePart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Denton on (029) 29.02.16.
 */
public class Field extends JPanel {
    private GUIView view;
    private EventListener eventListener;

    public Field(GUIView view) {
        this.view = view;
        KeyHandler keyHandler = new KeyHandler();
        addKeyListener(keyHandler);
        setFocusable(true);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, view.getContentPane().getWidth(), view.getContentPane().getHeight());

        // snake parts drawing
        int currX;
        int currY;
        int borderSize = 1;
        for (SnakePart currPart : view.getModel().getSnake().getSnakeParts()) {
            currX = currPart.getX() * GUIView.GUI_PIXEL_SIZE;
            currY = currPart.getY() * GUIView.GUI_PIXEL_SIZE;
            g.setColor(Color.WHITE);
            g.fillRect(currX, currY, GUIView.GUI_PIXEL_SIZE, GUIView.GUI_PIXEL_SIZE);
            g.setColor(Color.RED);
            g.fillRect(currX + borderSize, currY + borderSize, GUIView.GUI_PIXEL_SIZE - 2 * borderSize, GUIView.GUI_PIXEL_SIZE - 2 * borderSize);
        }

        // mouse drawing
        currX = view.getModel().getMouse().getX() * GUIView.GUI_PIXEL_SIZE;
        currY = view.getModel().getMouse().getY() * GUIView.GUI_PIXEL_SIZE;
        g.setColor(Color.WHITE);
        g.fillRect(currX, currY, GUIView.GUI_PIXEL_SIZE, GUIView.GUI_PIXEL_SIZE);
        g.setColor(Color.GREEN);
        g.fillRect(currX + borderSize, currY + borderSize, GUIView.GUI_PIXEL_SIZE - 2 * borderSize, GUIView.GUI_PIXEL_SIZE - 2 * borderSize);
    }

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public class KeyHandler extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            switch (key) {
                case KeyEvent.VK_LEFT:
                    eventListener.onChangeDirectionEvent(Direction.LEFT);
                    break;
                case KeyEvent.VK_RIGHT:
                    eventListener.onChangeDirectionEvent(Direction.RIGHT);
                    break;
                case KeyEvent.VK_UP:
                    eventListener.onChangeDirectionEvent(Direction.UP);
                    break;
                case KeyEvent.VK_DOWN:
                    eventListener.onChangeDirectionEvent(Direction.DOWN);
                    break;
            }
        }
    }
}
