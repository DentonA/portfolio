package projects.snake.view;

import projects.snake.controller.EventListener;
import projects.snake.model.Model;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Denton on (029) 29.02.16.
 */
public class GUIView extends JFrame implements View {
    public static final int GUI_PIXEL_SIZE = 20;

    private EventListener eventListener;
    private Model model;
    private Field field;

    public GUIView(Model model) {
        this.model = model;
    }

    public void init() {
        field = new Field(this);
        add(field);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setPreferredSize(new Dimension(model.getWidth() * GUI_PIXEL_SIZE, model.getHeight() * GUI_PIXEL_SIZE));
        pack();
        setLocationRelativeTo(null);
        setTitle("Snake");
        setVisible(true);
    }

    public void update() {
        field.repaint();
    }

    public void finishGame() {
        JOptionPane.showMessageDialog(this, "Game over!");
    }

    public void setEventListener(EventListener eventListener) {
        field.setEventListener(eventListener);
    }

    protected Model getModel() {
        return model;
    }
}
